package br.edu.ifpb.memoriam.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.edu.ifpb.memoriam.entity.Contato;
import br.edu.ifpb.memoriam.entity.Operadora;
import br.edu.ifpb.memoriam.entity.Usuario;
import br.edu.ifpb.memoriam.facade.ContatoController;
import br.edu.ifpb.memoriam.facade.LoginController;
import br.edu.ifpb.memoriam.facade.OperadoraController;
import br.edu.ifpb.memoriam.facade.Resultado;
import br.edu.ifpb.memoriam.facade.UsuarioController;

/**
 * Servlet implementation class FrontControllerServlet
 */
@WebServlet("/controller.do")
public class FrontControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public FrontControllerServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		Usuario usuario = (Usuario) session.getAttribute("usuario");		
		ContatoController contatoCtrl = new ContatoController();
		OperadoraController operadoraCtrl = new OperadoraController();
		UsuarioController usuarioCtrl = new UsuarioController();
		String proxPagina = null;
		
		//se n�o existir usu�rio logado ele retorna para a p�gina de Login
		if(usuario == null) {
			response.sendRedirect("login/login.jsp");
			return;
		}
		
		String id;
		String nome;
		this.getServletContext().removeAttribute("msgs");
		String operacao = request.getParameter("op");

		String pesquisa;
		switch (operacao) {

		case "edtope":
			if (usuario.getPerfil().toString() == "BASIC") {
				response.sendRedirect("login/login.jsp");
				return;
			}
			id = request.getParameter("id");
			Operadora operadora = operadoraCtrl.buscar(Integer.parseInt(id));
			request.setAttribute("operadora", operadora);
			proxPagina = "operadora/editar.jsp";
			break;

		case "edtusu":
			if (usuario.getPerfil().toString() == "BASIC") {
				response.sendRedirect("login/login.jsp");
				return;
			}
			id = request.getParameter("id");
			Usuario usuario1 = usuarioCtrl.buscar(Integer.parseInt(id));
			request.setAttribute("usuario", usuario1);
			proxPagina = "usuario/editar.jsp";
			break;

		case "edtctt":
			id = request.getParameter("id");
			Contato contato = contatoCtrl.buscar(Integer.parseInt(id));
			request.setAttribute("contato", contato);
			proxPagina = "contato/editar.jsp";
			break;

		case "conope":
			if (usuario.getPerfil().toString() == "BASIC") {
				response.sendRedirect("login/login.jsp");
				return;
			}
			pesquisa = request.getParameter("pesquisa");
			List<Operadora> operadoras = null;
			if (pesquisa != null) {
				operadoras = operadoraCtrl.pesquisar(pesquisa);
			} else {
				operadoras = operadoraCtrl.consultar();
			}			
			request.setAttribute("operadoras", operadoras);
			proxPagina = "operadora/consulta.jsp";
			break;

		case "conusu":
			if (usuario.getPerfil().toString() == "BASIC") {
				response.sendRedirect("login/login.jsp");
				return;
			}
			pesquisa = request.getParameter("pesquisa");
			List<Usuario> usuarios = null;
			if (pesquisa != null) {
				usuarios = usuarioCtrl.pesquisar(pesquisa);
			} else {
				usuarios = usuarioCtrl.consultar();
			}			
			request.setAttribute("usuarios", usuarios);
			proxPagina = "usuario/consulta.jsp";
			break;

		case "conctt":
			pesquisa = request.getParameter("pesquisa");
			List<Contato> contatos = null;
			if (pesquisa != null) {
				contatos = contatoCtrl.pesquisar(pesquisa, usuario);
			} else {
				contatos = contatoCtrl.consultar(usuario);
			}
			request.setAttribute("contatos", contatos);
			proxPagina = "contato/consulta.jsp";
			break;
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher(proxPagina);
		dispatcher.forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		this.getServletContext().removeAttribute("msgs");
		String operacao = request.getParameter("op");
				
		if (operacao == null) {
			this.getServletContext().setAttribute("msgs",
					new String[] { "Opera��o (op) n�o especificada na requisi��o!" });
			response.sendRedirect(request.getHeader("Referer"));
			return;
		}

		OperadoraController operadoraCtrl = new OperadoraController();
		ContatoController contatoCtrl = new ContatoController();
		LoginController loginCtrl = new LoginController();
		UsuarioController usuarioCtrl = new UsuarioController();
		Resultado resultado = null;

		String paginaSucesso = "controller.do?op=conctt";
		String paginaSucessoOpe = "controller.do?op=conope";
		String paginaSucessoUsu = "controller.do?op=conusu";
		String paginaErro = "contato/cadastro.jsp";
		String paginaErroOpe = "operadora/cadastro.jsp";
		String paginaErroUsu = "usuario/cadastro.jsp";
		String proxPagina = null;
		String proxLogout = "login/login.jsp";
		String nome;
		switch (operacao) {
		case "login":
			paginaSucesso = "controller.do?op=conctt";
			paginaErro = "login/login.jsp";
			resultado = loginCtrl.isValido(request.getParameterMap());
			if (resultado.isErro()) {
				request.setAttribute("msgs", resultado.getMensagens());
				proxPagina = paginaErro;
			} else {
				session.setAttribute("usuario", (Usuario) resultado.getEntidade());
				proxPagina = paginaSucesso;
				String lembrar = request.getParameter("lembrar");
				if (lembrar != null) {
					Usuario usuariologado = new Usuario();
					Cookie c = new Cookie("loginCookie", usuariologado.getEmail());
					c.setMaxAge(-1);
					response.addCookie(c);
				} else {
					for (Cookie cookie : request.getCookies()) {
						if (cookie.getName().equals("loginCookie")) {
							cookie.setValue(null);
							cookie.setMaxAge(0);
							response.addCookie(cookie);

						}

					}
				}
			}
			break;
		case "cadctt":
			resultado = contatoCtrl.cadastrar(request.getParameterMap());
			if (!resultado.isErro()) {
				proxPagina = paginaSucesso;
				this.getServletContext().setAttribute("msgs", resultado.getMensagens());
				request.setAttribute("msgs", resultado.getMensagens());
			} else {
				request.setAttribute("contato", (Contato) resultado.getEntidade());
				request.setAttribute("msgs", resultado.getMensagens());
				proxPagina = paginaErro;
			}
			break;
		case "deletectt":
			resultado = contatoCtrl.remove(request.getParameterMap());
			if (!resultado.isErro()) {
				proxPagina = paginaSucesso;
				request.setAttribute("msgs", resultado.getMensagens());
			} else {
				request.setAttribute("contato", (Contato) resultado.getEntidade());
				request.setAttribute("msgs", resultado.getMensagens());
				proxPagina = paginaErro;
			}

			break;
		case "deleteusu":
			if (usuario.getPerfil().toString() == "BASIC") {
				response.sendRedirect("login/login.jsp");
				return;
			}
			resultado = usuarioCtrl.remove(request.getParameterMap());
			if (!resultado.isErro()) {
				proxPagina = paginaSucessoUsu;
				request.setAttribute("msgs", resultado.getMensagens());
			} else {
				request.setAttribute("usuario", (Usuario) resultado.getEntidade());
				request.setAttribute("msgs", resultado.getMensagens());
				proxPagina = paginaErroUsu;
			}

			break;
		case "deleteope":
			if (usuario.getPerfil().toString() == "BASIC") {
				response.sendRedirect("login/login.jsp");
				return;
			}
			resultado = operadoraCtrl.remove(request.getParameterMap());
			if (!resultado.isErro()) {
				proxPagina = paginaSucessoOpe;
				request.setAttribute("msgs", resultado.getMensagens());
			} else {
				request.setAttribute("operadora", (Operadora) resultado.getEntidade());
				request.setAttribute("msgs", resultado.getMensagens());
				proxPagina = paginaErroOpe;
			}

			break;
		case "cadope":
			if (usuario.getPerfil().toString() == "BASIC") {
				response.sendRedirect("login/login.jsp");
				return;
			}
			resultado = operadoraCtrl.cadastrar(request.getParameterMap());
			if (!resultado.isErro()) {
				proxPagina = paginaSucessoOpe;
				this.getServletContext().setAttribute("msgs", resultado.getMensagens());
				request.setAttribute("msgs", resultado.getMensagens());
			} else {
				request.setAttribute("operadora", (Operadora) resultado.getEntidade());
				request.setAttribute("msgs", resultado.getMensagens());
				proxPagina = paginaErroOpe;
			}
			break;
		case "cadusu":
			if (usuario.getPerfil().toString() == "BASIC") {
				response.sendRedirect("login/login.jsp");
				return;
			}
			resultado = usuarioCtrl.cadastrar(request.getParameterMap());
			if (!resultado.isErro()) {
				proxPagina = paginaSucessoUsu;
				this.getServletContext().setAttribute("msgs", resultado.getMensagens());
				request.setAttribute("msgs", resultado.getMensagens());
			} else {
				request.setAttribute("usuario", (Usuario) resultado.getEntidade());
				request.setAttribute("msgs", resultado.getMensagens());
				proxPagina = paginaErroUsu;
			}
			break;
		case "logout":
			resultado = new Resultado();
			proxPagina = proxLogout;
			session.invalidate();
			resultado.setErro(false);
			break;
		default:
			request.setAttribute("erro", "Opera��o n�o especificada no servlet!");
			proxPagina = "../erro/erro.jsp";
		}
		if (resultado.isErro()) {
			RequestDispatcher dispatcher = request.getRequestDispatcher(proxPagina);
			dispatcher.forward(request, response);
		} else {
			response.sendRedirect(proxPagina);
		}

	}
}
