package br.edu.ifpb.memoriam.facade;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import br.edu.ifpb.memoriam.dao.ContatoDAO;
import br.edu.ifpb.memoriam.dao.PersistenceUtil;
import br.edu.ifpb.memoriam.dao.UsuarioDAO;
import br.edu.ifpb.memoriam.entity.Contato;
import br.edu.ifpb.memoriam.entity.Perfil;
import br.edu.ifpb.memoriam.entity.Usuario;
import br.edu.ifpb.memoriam.util.PasswordUtil;

public class UsuarioController {

	private Usuario usuario;
	private List<Mensagem> mensagensErro;

	public List<Usuario> consultar() {
		UsuarioDAO dao = new UsuarioDAO(PersistenceUtil.getCurrentEntityManager());
		//List<Usuario> usuarios = dao.findAll();
		//mostra apenas os usu�rios ativos
		List<Usuario> usuarios = dao.listarAtivos();
		return usuarios;
	}
	
	public List<Usuario> pesquisar(String pesquisa) {
		UsuarioDAO dao = new UsuarioDAO(PersistenceUtil.getCurrentEntityManager());
		List<Usuario> usuarios = dao.pesquisar(pesquisa);
		return usuarios;
	}
	
	public List<Usuario> listaPerfil() {
		UsuarioDAO dao = new UsuarioDAO(PersistenceUtil.getCurrentEntityManager());
		List<Usuario> perfil = dao.listaPerfil();
		return perfil;
	}

	public Usuario buscar(int id) {
		UsuarioDAO dao = new UsuarioDAO(PersistenceUtil.getCurrentEntityManager());
		return dao.find(id);

	}

	public Resultado cadastrar(Map<String, String[]> parametros) {
		Resultado resultado = new Resultado();
		if (isParametrosValidos(parametros)) {
			UsuarioDAO dao = new UsuarioDAO(PersistenceUtil.getCurrentEntityManager());
			dao.beginTransaction();
			if (this.usuario.getId() == null) {
				dao.insert(this.usuario);
			} else {
				dao.update(this.usuario);
			}
			dao.commit();

			resultado.setErro(false);
			resultado.setMensagens(
					Collections.singletonList(new Mensagem("Usu�rio criado com sucesso", Categoria.INFO)));
		} else {
			resultado.setEntidade(this.usuario);
			resultado.setErro(true);
			resultado.setMensagens(this.mensagensErro);
		}
		return resultado;
	}

	private boolean isParametrosValidos(Map<String, String[]> parametros) {				
		String[] id = parametros.get("id");
		String[] nome = parametros.get("nome");
		String[] email = parametros.get("email");
		String[] senha = parametros.get("senha");
		String[] perfil = parametros.get("perfil");

		this.usuario = new Usuario();
		usuario.setAtivo(true);		
		this.mensagensErro = new ArrayList<Mensagem>();

		if (id != null && id.length > 0 && !id[0].isEmpty()) {
			usuario.setId(Integer.parseInt(id[0]));
		}

		if (email == null || email.length == 0 || email[0].isEmpty()) {
			this.mensagensErro.add(new Mensagem("Email - campo obrigat�rio!", Categoria.ERRO));
		} else {
			usuario.setEmail(email[0]);
		}

		if (nome == null || nome.length == 0 || nome[0].isEmpty()) {
			this.mensagensErro.add(new Mensagem("Nome - campo obrigat�rio!", Categoria.ERRO));
		} else {
			usuario.setNome(nome[0]);
		}

		if (senha == null || senha.length == 0 || senha[0].isEmpty()) {
			this.mensagensErro.add(new Mensagem("Senha - campo obrigat�rio!", Categoria.ERRO));
		} else {
			usuario.setSenha(PasswordUtil.encryptMD5(senha[0]));
		}
		if (perfil == null || perfil.length == 0 || perfil[0].isEmpty()) {
			this.mensagensErro.add(new Mensagem("Campo - campo obrigat�rio!", Categoria.ERRO));
		} else {			
			if (perfil[0].equals("BASIC")) {
				usuario.setPerfil(Perfil.BASIC);
			} else {
				usuario.setPerfil(Perfil.ADMIN);
			}
		}

		return this.mensagensErro.isEmpty();
	}

	public Resultado remove(Map<String, String[]> parametros) {
		Resultado resultado = new Resultado();
		UsuarioDAO dao = new UsuarioDAO(PersistenceUtil.getCurrentEntityManager());
		dao.beginTransaction();
		String[] selecionadosform = parametros.get("del_selected");
		try {
			for (String s : selecionadosform) {
				Usuario c = dao.find(Integer.parseInt(s));
				//dao.delete(c);	
				c.setAtivo(false);
				//n�o deleta o usu�rio mais seta como inativo para n�o perder a refer�ncia das operadoras e contatos
				dao.update(c);
			}
			resultado.setErro(false);
			resultado.setMensagens(
					Collections.singletonList(new Mensagem("Usu�rio removido com sucesso", Categoria.INFO)));
		} catch (Exception exc) {
			resultado.setEntidade(this.usuario);
			resultado.setErro(true);
		}
		dao.commit();

		return resultado;
	}
}