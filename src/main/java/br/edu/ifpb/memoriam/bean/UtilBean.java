package br.edu.ifpb.memoriam.bean;
import java.util.List;


import br.edu.ifpb.memoriam.dao.OperadoraDAO;
import br.edu.ifpb.memoriam.dao.PersistenceUtil;
import br.edu.ifpb.memoriam.dao.UsuarioDAO;
import br.edu.ifpb.memoriam.entity.Operadora;
import br.edu.ifpb.memoriam.entity.Usuario;

public class UtilBean {
	public List<Operadora> getOperadoras() {
		OperadoraDAO dao = new OperadoraDAO(PersistenceUtil.getCurrentEntityManager());
		List<Operadora> operadoras = dao.findAll();
		return operadoras;
	}
	
	public List<Usuario> getUsuarios() {
		UsuarioDAO cdao = new UsuarioDAO(PersistenceUtil.getCurrentEntityManager());
		List<Usuario> usuarios = cdao.findAll();
		return usuarios;
	}
	
	public boolean isNumeric(String s) {  
	    return s != null && s.matches("[-+]?\\d*\\.?\\d+");  
	}
}