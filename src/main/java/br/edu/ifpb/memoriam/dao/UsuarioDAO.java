package br.edu.ifpb.memoriam.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.edu.ifpb.memoriam.bean.UtilBean;
import br.edu.ifpb.memoriam.entity.Contato;
import br.edu.ifpb.memoriam.entity.Usuario;

public class UsuarioDAO extends GenericDAO<Usuario, Integer> {

	public UsuarioDAO() {
		this(PersistenceUtil.getCurrentEntityManager());
	}

	public UsuarioDAO(EntityManager em) {
		super(em);
	}

	public Usuario findByLogin(String login) {
		Query q = this.getEntityManager().createQuery("select u from Usuario u where u.email = :login");
		q.setParameter("login", login);
		Usuario usuario = null;
		try {
			usuario = (Usuario) q.getSingleResult();
		} catch (NoResultException e) {
		}
		return usuario;
	}

	public List<Usuario> listaPerfil() {
		Query q = this.getEntityManager().createQuery("select u from Usuario u group by u.perfil");
		return q.getResultList();
	}

	public List<Usuario> pesquisar(String pesquisa) {
		Query q = null;
		q = this.getEntityManager().createQuery("from Usuario u where LOWER(u.nome) LIKE :nome or LOWER(u.email) LIKE :email");
		q.setParameter("nome", "%" + pesquisa.toLowerCase() + "%");
		q.setParameter("email", "%" + pesquisa.toLowerCase() + "%");
		return q.getResultList();
	}
	
	public List<Usuario> listarAtivos() {
		Query q = this.getEntityManager().createQuery("select u from Usuario u where u.ativo = TRUE");
		return q.getResultList();
	}
	
}
