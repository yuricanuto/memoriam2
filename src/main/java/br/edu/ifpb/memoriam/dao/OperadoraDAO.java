package br.edu.ifpb.memoriam.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.edu.ifpb.memoriam.bean.UtilBean;
import br.edu.ifpb.memoriam.entity.Contato;
import br.edu.ifpb.memoriam.entity.Operadora;
import br.edu.ifpb.memoriam.entity.Usuario;

public class OperadoraDAO extends GenericDAO<Operadora, Integer> {
	
	public OperadoraDAO() {
		this(PersistenceUtil.getCurrentEntityManager());
	}

	public OperadoraDAO(EntityManager em) {
		super(em);
	}
	
	public List<Operadora> pesquisar(String pesquisa) {
		UtilBean util = new UtilBean();
		Query q = null;
		if (util.isNumeric(pesquisa)) {
			q = this.getEntityManager()
					.createQuery("from Operadora o where cast(o.prefixo as string) LIKE :prefixo"); 
			q.setParameter("prefixo", "%" + pesquisa + "%");  
		} else {
			q = this.getEntityManager()
					.createQuery("from Operadora c where LOWER(c.nome) LIKE :nome");
			q.setParameter("nome", "%" + pesquisa.toLowerCase() + "%");
		}
		return q.getResultList();
	}

}
