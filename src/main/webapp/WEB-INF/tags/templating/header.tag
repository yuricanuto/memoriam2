<%@tag description="Barra de navega��o comum �s p�ginas"
	body-content="empty"%>
<%@ attribute name="pagina" required="false"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="perfil" value="${sessionScope.usuario.perfil}" />
<c:set var="nome" value="${sessionScope.usuario.nome}" />
<nav class="navbar navbar-inverse">
	<div class="container">
		<div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="${pageContext.request.contextPath}/controller.do?op=conctt"">Memori<i class="glyphicon glyphicon-phone"></i>m</a>
	    </div>
		<div id="navbar">
			<c:if test="${perfil eq 'ADMIN'}">
				<ul class="nav navbar-nav">
					<li class="${pagina eq 'contatos' ? 'active' : ''}"><a
						href="${pageContext.request.contextPath}/controller.do?op=conctt">Contatos</a>
					</li>
					<li class="${pagina eq 'operadoras' ? 'active' : ''}"><a
						href="${pageContext.request.contextPath}/controller.do?op=conope">Operadoras</a></li>

					<li class="${pagina eq 'usuarios' ? 'active' : ''}"><a
						href="${pageContext.request.contextPath}/controller.do?op=conusu">Usuarios</a></li>
				</ul>
			</c:if>
			<c:if test="${perfil eq 'BASIC'}">
				<ul class="nav navbar-nav">
					<li class="${pagina eq 'contatos' ? 'active' : ''}"><a
						href="${pageContext.request.contextPath}/controller.do?op=conctt">Contatos</a>
					</li>
				</ul>
			</c:if>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false">${nome} (${perfil}) <span class="caret"></span>
				</a>
				<ul class="dropdown-menu">
					<li><a href="#" id="link-submit"> <i
							class="glyphicon glyphicon-log-out"></i>Sair
					</a></li>
				</ul></li>
			</ul>
		</div>
	</div>
</nav>
<form id="logout-form" action="controller.do" method="POST">
	<input type="hidden" name="op" value="logout" />
</form>
<script type="text/javascript">
	var form = document.getElementById("logout-form");
	document.getElementById("link-submit").addEventListener("click",
			function() {
				form.submit();
			});
</script>