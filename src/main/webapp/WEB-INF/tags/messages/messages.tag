<%@ tag description="Tag apra exibir mensagens diversas"
	pageEncoding="UTF-8"%>
<%@ tag import="br.edu.ifpb.memoriam.facade.Categoria"%>
<%@ tag import="br.edu.ifpb.memoriam.facade.Mensagem"%>
<%@ attribute name="value" required="true" rtexprvalue="true"
	type="java.util.List"%>
<%@ attribute name="erroStyle" required="false" rtexprvalue="true"%>
<%@ attribute name="infoStyle" required="false" rtexprvalue="true"%>
<%@ attribute name="avisoStyle" required="false" rtexprvalue="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${not empty value}">
	<div align="left" style="width: 50%">
		<div>
			<c:set var="tipo" value="${value.get(0).categoria }" />
			<c:choose>
				<c:when test="${tipo eq 'ERRO'}">
					<ul class="alert alert-danger">
				</c:when>

				<c:when test="${ tipo eq 'AVISO'}">
					<ul class="alert alert-warning">
				</c:when>

				<c:when test="${ tipo eq 'INFO'}">
					<ul class="alert alert-success">
				</c:when>
				<c:otherwise>
					<ul class="alert alert-info">
				</c:otherwise>
			</c:choose>

			<c:forEach var="msg" items="${value}">
				<li style="list-style-type: none;">${msg.mensagem}</li>
			</c:forEach>
			</ul>
		</div>
	</div>
</c:if>