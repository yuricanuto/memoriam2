
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tt" tagdir="/WEB-INF/tags/templating"%>
<%@ taglib prefix="mm" tagdir="/WEB-INF/tags/messages"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<tt:template title="Editar - operadora" pagina="operadoras">
	<jsp:attribute name="tscript">
<link
			href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css"
			rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/memoriam.css"
			rel="stylesheet">
<script
			src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
</jsp:attribute>
	<jsp:body>
	<div class="container">		
			<h2>
				Memori<i class="glyphicon glyphicon-phone"></i>m
			</h2>

			<h4>Dados da Operadora</h4>
			<c:if test="${not empty msgs}">
				<div align="left">
					<div style="color: red">
						<ul style="padding-left: 0px;">
							<c:forEach var="msg" items="${msgs}">
								<li style="list-style-type: none;">${msg}</li>
							</c:forEach>
						</ul>
					</div>
				</div>
			</c:if>
			<br>

			<form action="${pageContext.request.contextPath}/controller.do" style="width: 50%"
					method="post" class="form-horizontal">
				<input type="hidden" name="op" value="cadope"> 
				<input type="hidden" name="id" value="${operadora.id}">
				<input id="nome" value="${operadora.nome}" name="nome" type="text"
						class="form-control" /> 
				<input id="prefixo" value="${operadora.prefixo}" name="prefixo"
						type="text" class="form-control" />	
				
				<div class="row">
					<div class="col-md-6">
						<input type="submit" class="form-control btn btn-primary"
								value="Salvar">
					</div>
					<div class="col-md-6">
						<a class="form-control btn btn-success"
								href="${pageContext.request.contextPath}/controller.do?op=conope">Voltar</a>
					</div>
				</div>
			</form>
					
	</div>
	<c:set var="endofconversation" value="true" scope="request" />
	
	</jsp:body>
</tt:template>