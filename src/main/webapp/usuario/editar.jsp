<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tt" tagdir="/WEB-INF/tags/templating"%>
<%@ taglib prefix="mm" tagdir="/WEB-INF/tags/messages"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<tt:template title="Editar - usuário" pagina="usuarios">
	<jsp:attribute name="tscript">
<link
			href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css"
			rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/memoriam.css"
			rel="stylesheet">
<script
			src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
</jsp:attribute>
	<jsp:body>
	<div class="container">		
			<h2>
				Memori<i class="glyphicon glyphicon-phone"></i>m
			</h2>
			<h4>Dados do Usuario</h4>
			<c:if test="${not empty msgs}">
				<div align="left">
					<div style="color: red">
						<ul style="padding-left: 0px;">
							<c:forEach var="msg" items="${msgs}">
								<li style="list-style-type: none;">${msg}</li>
							</c:forEach>
						</ul>
					</div>
				</div>
			</c:if>

			<form action="${pageContext.request.contextPath}/controller.do" style="width: 50%"
					method="post" class="form-horizontal">
				<input type="hidden" name="op" value="cadusu"> 
				<input type="hidden" name="id" value="${usuario.id}">
				<input id="nome" value="${usuario.nome}" name="nome" type="text"
						class="form-control" /> 
				<input id="email" value="${usuario.email}" name="email" type="text"
						class="form-control" />
				<select id="perfil" class="form-control" name="perfil" required>
                	<option value="${null}" label="Selecione o perfil">
						Selecione o perfil</option>				
					<option value="BASIC" ${usuario.perfil eq 'BASIC' ? 'selected' : ''}>Básico</option>						
					<option value="ADMIN" ${usuario.perfil eq 'ADMIN' ? 'selected' : ''}>Administrador</option>	
				</select>
				<input type="password" name="senha" class="form-control" value=""
						id="senha" placeholder="Senha">				
				 
				<div class="row">
					<div class="col-md-6">
						<input type="submit" class="form-control btn btn-primary"
									value="Salvar">
					</div>
					<div class="col-md-6">
						<a class="form-control btn btn-success"
									href="${pageContext.request.contextPath}/controller.do?op=conusu">Voltar</a>
					</div>
				</div>
			</form>	
	</div>
	<c:set var="endofconversation" value="true" scope="request" />
	
	</jsp:body>
</tt:template>