<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tt" tagdir="/WEB-INF/tags/templating"%>
<%@ taglib prefix="mm" tagdir="/WEB-INF/tags/messages"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<tt:template title="Cadastro - usuários" pagina="usuarios">
	<jsp:attribute name="tscript">
	<script
			src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link
			href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css"
			rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/css/memoriam.css"
			rel="stylesheet">
</jsp:attribute>
	<jsp:body>

	<div class="container">				
			<h2>
				Memori<i class="glyphicon glyphicon-phone"></i>m
			</h2>
			<h4>Dados do usuario</h4>			
			<mm:messages value="${msgs}" erroStyle="color:red"
				infoStyle="color:blue" />					
			<form action="${pageContext.request.contextPath}/controller.do"
				style="width: 50%" method="POST" class="form-horizontal" id="myForm">
				<input type="hidden" name="op" value="cadusu">
				<input id="nome" name="nome" type="text" class="form-control"
					placeholder="Nome" />
				<input id="email " type="text" name="email" class="form-control"
					placeholder="Email">						
						
	 			<select class="form-control" id="perfil" name="perfil">
					<option value="${null}" label="Selecione o perfil">
						Selecione o perfil</option>				
					<option value="BASIC">Básico</option>						
					<option value="ADMIN">Administrador</option>	
				</select> 
                
				<input type="password" name="senha" class="form-control" value=""
					id="senha" placeholder="Senha">
				
				<div class="row">
					<div class="col-md-6">
						<input type="submit" class="form-control btn btn-primary"
							value="Salvar">
					</div>
					<div class="col-md-6">
						<a class="form-control btn btn-success"
							href="${pageContext.request.contextPath}/controller.do?op=conusu">Voltar</a>
					</div>
				</div>	

			</form>
		
	</div>
	<c:set var="endofconversation" value="true" scope="request" />
	</jsp:body>
</tt:template>