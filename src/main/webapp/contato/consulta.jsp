<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tt" tagdir="/WEB-INF/tags/templating"%>
<%@ taglib prefix="mm" tagdir="/WEB-INF/tags/messages"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<tt:template title="Consulta - contatos" pagina="contatos">


	<jsp:attribute name="tscript">
<link
			href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css"
			rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/memoriam.css"
			rel="stylesheet">
</jsp:attribute>

	<jsp:body>
	<div class="container">		
	
			<div class="row">
				<div class="col-md-6">
					<h2>			
						Memori<i class="glyphicon glyphicon-phone"></i>m
					</h2>
				</div>
				<div class="col-md-6">
					<div style="text-align: right; padding-top: 30px;">	
						 <form action="${pageContext.request.contextPath}/controller.do?"
							method="GET">
						 <input type="hidden" name="op" value="conctt">		         
				         	<div class="input-group">
						      <input class="form-control" type="text" id="pesquisa"
									name="pesquisa" value="" />
						      <span class="input-group-btn">
						        <button class="btn btn-default" type="submit">Pesquisar</button>
						      </span>
					    	</div>
							<br />        
				         </form>

			        </div>
				</div>
			</div>	
			<mm:messages value="${msgs}" erroStyle="color:red"
				infoStyle="color:blue" />
			<form
				action="${pageContext.request.contextPath}/controller.do?op=deletectt"
				id="form_del" method="POST">
			<input type="hidden" name="op" value="cadctt">
			<table class="table table-striped table-bordered">
			
				<tr align="left">
					<th style="width: 25px;"></th>
					<th>Nome</th>
					<th>Telefone</th>
					<th>Operadora</th>
				</tr>	
				
				<c:if test="${empty contatos}">
					<tr align="left">
						<td align="center" colspan="4">Nenhum Resultado Encontrado</td>
					</tr>
				</c:if>
				<c:if test="${not empty contatos}">
					<c:forEach var="contato" items="${contatos}">
					<tr align="left">
						<td align="center"><input class="selections"
								name="del_selected" type="checkbox" value="${contato.id}" /></td>
						<td><a href="controller.do?op=edtctt&id=${contato.id}">${contato.nome}</a></td>
						<td>${contato.fone}</td>
						<td>${contato.operadora.nome}</td>
					</tr>
				</c:forEach>	
				</c:if>
		
				</table>
				</form>	
										
			<div class="row">
				<div class="col-md-3">
					<a href="contato/cadastro.jsp" class="form-control btn btn-primary">Novo Contato</a>					
				</div>
				<div class="col-md-3">
					<a class="form-control btn btn-danger btn_delete"
						style="display: none;">Apagar selecionados</a>
				</div>
			</div>
					
		
	</div>
	<form id="form_oculto" action="" style="display: none;"></form>

<script
			src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script
			src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
<script
			src="${pageContext.request.contextPath}/bootstrap/dist/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css"
			href="${pageContext.request.contextPath}/bootstrap/dist/sweetalert.css">
<script>
	$(document)
			.ready(
					function() {

						$("#pesquisa").popover({
							title : 'Tipos de Pesquisa',
							content : "Pesquise por Nome ou Telefone",
							placement : "left"
						}).blur(function() {
							$(this).popover('hide');
						});

						$(".selections").change(function() {
							if ($('.selections:checkbox:checked').length > 0) {
								$(".btn_delete").show();
								$(".btn_novo").hide();
							} else {
								$(".btn_delete").hide();
								$(".btn_novo").show();
							}

						});

						$(".btn_delete")
								.click(
										function() {
											selecionados = $('.selections:checkbox:checked');
											swal(
													{
														title : "Você tem certeza?",
														text : "Você não sera capaz de recuperar esses dados!",
														type : "warning",
														showCancelButton : true,
														confirmButtonColor : "#DD6B55",
														confirmButtonText : "Sim, delete!",
														cancelButtonText : "Não, cancele!",
														closeOnConfirm : false,
														closeOnCancel : false
													},
													function(isConfirm) {
														if (isConfirm) {
															swal(
																	"Deletado!",
																	"Dados deletados com sucesso.",
																	"success");
															$("#form_del")
																	.submit();
														} else {
															swal(
																	"Cancelado",
																	"Voce cancelou a operação :)",
																	"error");
														}
													});
										});
					});
</script>
</jsp:body>
</tt:template>
