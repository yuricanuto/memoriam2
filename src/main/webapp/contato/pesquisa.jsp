<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tt" tagdir="/WEB-INF/tags/templating"%>
<%@ taglib prefix="mm" tagdir="/WEB-INF/tags/messages"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<tt:template title="Resultado da consulta">
<jsp:attribute name="tscript">
<link
	href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/memoriam.css"
	rel="stylesheet">
</jsp:attribute>
<jsp:body>
	<div class="container">
		<div class="jumbotron">
			<h2>			
				Memori<i class="glyphicon glyphicon-phone"></i>m
			</h2>			
			<mm:messages value="${msgs}" erroStyle="color:red" infoStyle="color:blue" />
			<table class="table table-striped table-bordered">
				<tr align="left">
					
					<th style="width: 30%">Nome</th>
					<th>Telefone</th>
					<th>Operadora</th>
				</tr>				
				 <c:forEach var="contato" items="${contatospesq}">
					<tr align="left">
					
						<td>${contato.nome}</td>
						<td>${contato.fone}</td>
						<td>${contato.operadora.nome}</td>
					</tr>
				</c:forEach>
				</table>
				</form>
				<a class="form-control btn btn-success" style="width: 350px;" href="${pageContext.request.contextPath}/controller.do?op=conctt">Voltar</a>			
		</div>
	</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
</jsp:body>

</tt:template>
